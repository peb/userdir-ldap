#!/usr/bin/python

import datetime
import os
import shutil
import tempfile
import unittest

import ldap

import userdir_ldap.ldap
from userdir_ldap import UDLdap
from userdir_ldap import generate
from userdir_ldap.generate import (
    IsRetired, setup_group_maps, IsInGroup, GenPasswd, GenAllUsers,
    GenSSHGitolite, GenMailPassword)


class mock_ldapconn(object):
    def __init__(self, searchresults):
        self.searchresults = searchresults

    def search_s(self, dn, scope, filter, attrs):
        return self.searchresults[(dn, scope, filter)]


class TestGenerate(unittest.TestCase):
    def test_isretired(self):
        account = UDLdap.Account('uid=inactive',
                                 {'accountStatus': [b'inactive 2020-01-01']})
        self.assertTrue(IsRetired(account))
        account = UDLdap.Account('uid=memorial',
                                 {'accountStatus': [b'memorial 2020-01-01']})
        self.assertTrue(IsRetired(account))
        date = (datetime.date.today() - datetime.timedelta(days=200)).strftime('%Y-%m-%d').encode('ascii')
        account = UDLdap.Account('uid=old emeritus',
                                 {'accountStatus': [b'retiring %s' % date]})
        self.assertTrue(IsRetired(account))
        date = (datetime.date.today() - datetime.timedelta(days=150)).strftime('%Y-%m-%d').encode('ascii')
        account = UDLdap.Account('uid=new emeritus',
                                 {'accountStatus': [b'retiring %s' % date]})
        self.assertFalse(IsRetired(account))
        account = UDLdap.Account('uid=active', {})
        self.assertFalse(IsRetired(account))

    def test_isingroup(self):
        BaseDn = userdir_ldap.ldap.BaseDn
        results = {
            (BaseDn, ldap.SCOPE_ONELEVEL, "gid=*"): [
                ('gid=adm', {'gid': [b'adm'], 'gidNumber': [b'4']}),
                ('gid=Debian', {'gid': [b'Debian'], 'gidNumber': [b'800']}),
                ('gid=guest', {'gid': [b'guest'], 'gidNumber': [b'60000']}),
            ]}
        lc = mock_ldapconn(results)
        setup_group_maps(lc)
        account = UDLdap.Account('uid=admin', {'uid': [b'admin'], 'gidNumber': [b'4']})
        self.assertTrue(IsInGroup(account, ['4'], 'host'))
        self.assertFalse(IsInGroup(account, ['800'], 'host'))
        account = UDLdap.Account('uid=deb', {'uid': [b'deb'], 'gidNumber': [b'1000'], 'supplementaryGid': [b'Debian']})
        self.assertFalse(IsInGroup(account, ['4'], 'host'))
        self.assertTrue(IsInGroup(account, ['1000'], 'host'))
        self.assertFalse(IsInGroup(account, ['adm'], 'host'))
        self.assertTrue(IsInGroup(account, ['Debian'], 'host'))
        account = UDLdap.Account('uid=guest', {
            'uid': [b'guest'],
            'gidNumber': [b'1000'],
            'supplementaryGid': [b'guest'],
            'allowedHost': [b'guest.host']})
        self.assertFalse(IsInGroup(account, ['Debian'], 'host'))
        self.assertTrue(IsInGroup(account, ['Debian'], 'guest.host'))
        account = UDLdap.Account('uid=expired-guest', {
            'uid': [b'guest'],
            'gidNumber': [b'1000'],
            'supplementaryGid': [b'guest'],
            'allowedHost': [b'guest.host 20200101']})
        self.assertFalse(IsInGroup(account, ['Debian'], 'host'))
        self.assertFalse(IsInGroup(account, ['Debian'], 'guest.host'))
        date = (datetime.date.today() + datetime.timedelta(days=10)).strftime('%Y%m%d').encode('ascii')
        account = UDLdap.Account('uid=valid-guest', {
            'uid': [b'guest'],
            'gidNumber': [b'1000'],
            'supplementaryGid': [b'guest'],
            'allowedHost': [b'guest.host %s' % date]})
        self.assertFalse(IsInGroup(account, ['Debian'], 'host'))
        self.assertTrue(IsInGroup(account, ['Debian'], 'guest.host'))

    def test_genpasswd(self):
        accounts = [
            ('uid=invalid', {
                'uid': [b'invalid'],
                'uidNumber': [b'1000'],
                'gidNumber': [b'1000'],
                'gecos': [b''],
            }),
            ('uid=user', {
                'uid': [b'user'],
                'uidNumber': [b'1001'],
                'gidNumber': [b'1001'],
                'gecos': [b'\xe2\x98\xad'],
                'loginShell': [b'/bin/bash'],
            }),
        ]
        accounts = [UDLdap.Account(*a) for a in accounts]
        tmpdir = tempfile.mkdtemp()
        try:
            path = os.path.join(tmpdir, 'passwd')
            userlist = GenPasswd(accounts, path, '/home/', '!')
            self.assertEqual(userlist, {'user': 1001})
        finally:
            shutil.rmtree(tmpdir, ignore_errors=True)

    def test_genmailpassword(self):
        accounts = [
            # This one has no mailPassword and can't be in the file
            ('uid=invalid', {
                'uid': [b'invalid'],
                'uidNumber': [b'1000'],
                'gidNumber': [b'1000'],
            }),
            # This one is fine
            ('uid=user', {
                'uid': [b'user'],
                'uidNumber': [b'1001'],
                'gidNumber': [b'1001'],
                'mailPassword': [b'$6$aaaaaaaa$bbbb'],
                'userPassword': [b'{CRYPT}$6$aaa'],
            }),
            # This one is locked and shouldn't be there
            ('uid=locked_user', {
                'uid': [b'locked_user'],
                'uidNumber': [b'1002'],
                'gidNumber': [b'1002'],
                'mailPassword': [b'$6$aaaaaaaa$bbbb'],
                'userPassword': [b'{CRYPT}!$6$aaa'],
            }),
            # This one has no userPassword which is preventing mailPassword to work
            ('uid=nopass_user', {
                'uid': [b'nopass_user'],
                'uidNumber': [b'1003'],
                'gidNumber': [b'1003'],
                'mailPassword': [b'$6$aaaaaaaa$bbbb'],
            }),
            # This one is fine, too.
            ('uid=nice_user', {
                'uid': [b'nice_user'],
                'uidNumber': [b'1004'],
                'gidNumber': [b'1004'],
                'mailPassword': [b'$6$aaaaaaaa$cccc'],
                'userPassword': [b'{CRYPT}$6$aaa'],
            }),
            # This one is not fine
            ('uid=guest', {
                'uid': [b'guest'],
                'uidNumber': [b'1005'],
                'gidNumber': [b'1005'],
                'mailPassword': [b'$6$aaaaaaaa$cccc'],
                'userPassword': [b'{CRYPT}$6$aaa'],
                'supplementaryGid': [b'guest'],
            }),
        ]
        accounts = [UDLdap.Account(*a) for a in accounts]
        tmpdir = tempfile.mkdtemp()
        try:
            path = os.path.join(tmpdir, 'mail-passwords')
            GenMailPassword(accounts, path)
            with open(path, 'r') as mail_passwords:
                userlist = mail_passwords.readlines()
            self.assertEqual(userlist, ['user:$6$aaaaaaaa$bbbb\n', 'nice_user:$6$aaaaaaaa$cccc\n'])
        finally:
            shutil.rmtree(tmpdir, ignore_errors=True)

    def test_genallusers(self):
        accounts = [
            ('uid=invalid', {
                'uid': [b'invalid'],
                'uidNumber': [b'1000'],
                'gidNumber': [b'1000'],
                'gecos': [b''],
            }),
            ('uid=user', {
                'uid': [b'user'],
                'uidNumber': [b'1001'],
                'gidNumber': [b'1001'],
                'gecos': [b'\xe2\x98\xad'],
                'loginShell': [b'/bin/bash'],
            }),
        ]
        accounts = [UDLdap.Account(*a) for a in accounts]
        tmpdir = tempfile.mkdtemp()
        try:
            path = os.path.join(tmpdir, 'all-accounts.json')
            GenAllUsers(accounts, path)
        finally:
            shutil.rmtree(tmpdir, ignore_errors=True)

    def test_gensshgitolite(self):
        accounts = [{
            'uid': 'user',
            'sshRSAAuthKey': [b'ssh-rsa global',
                              b'allowed_hosts=foo.debian.org ssh-rsa only-for-foo']
        }]
        hosts = ()
        generate.GitoliteSSHRestrictions = 'command="@@COMMAND@@",restrict'
        tmpdir = tempfile.mkdtemp()
        try:
            path = os.path.join(tmpdir, "sshgitolite")
            GenSSHGitolite(accounts, hosts, path, "sshcommand @@USER@@")
            with open(path) as f:
                self.assertEqual(f.read(), 'command="sshcommand user",restrict ssh-rsa global\n')
            path = os.path.join(tmpdir, "sshgitolite-foo")
            GenSSHGitolite(accounts, hosts, path, "sshcommand @@USER@@", "foo.debian.org")
            with open(path) as f:
                self.assertEqual(f.read(), 'command="sshcommand user",restrict ssh-rsa global\n'
                                           'command="sshcommand user",restrict ssh-rsa only-for-foo\n')
            path = os.path.join(tmpdir, "sshgitolite-bar")
            GenSSHGitolite(accounts, hosts, path, "sshcommand @@USER@@", "bar")
            with open(path) as f:
                self.assertEqual(f.read(), 'command="sshcommand user",restrict ssh-rsa global\n')
        finally:
            shutil.rmtree(tmpdir, ignore_errors=True)


if __name__ == '__main__':
    unittest.main()

# vim: set et sw=4 sts=4:
