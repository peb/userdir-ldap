#!/usr/bin/python3

# Copyright (c) 2015 Peter Palfrader <peter@palfrader.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

import argparse
import sys

import ldap

from userdir_ldap.ldap import ConfModule, connectLDAP, BaseBaseDn


DefaultGroup = getattr(ConfModule, "defaultgroup", 'users')

parser = argparse.ArgumentParser(description='Upgrade a guest account.')
parser.add_argument('uid', metavar='UID',
                    help="user's uid to be upgraded")
args = parser.parse_args()
uid = args.uid

lc = connectLDAP()

x = lc.search_s(BaseBaseDn, ldap.SCOPE_SUBTREE, "uid=" + uid, [])
if len(x) == 0:
    print("No hit.", file=sys.stderr)
    sys.exit(1)
elif len(x) > 1:
    print("More than one hit!?", file=sys.stderr)
    sys.exit(1)


dn = x[0][0]
attrs = x[0][1]

keys = sorted(attrs.keys())
print("Current info:", file=sys.stderr)
print(dn, file=sys.stderr)
for a in keys:
    for i in attrs[a]:
        print("  {:<16}: {}".format(a, i.decode("utf-8")))

if 'supplementaryGid' not in attrs or b'guest' not in attrs['supplementaryGid']:
    print("Account is not a guest-account,")
    sys.exit(1)

print()
print()
print("dn:", dn)
print("changetype: modify")
print("delete: allowedHost")
print("-")
print("delete: shadowExpire")
print("-")
print("replace: supplementaryGid")
for gid in attrs['supplementaryGid']:
    if gid == b"guest":
        gid = DefaultGroup.encode("utf-8")
    print("supplementaryGid:", gid.decode("utf-8"))
print("-")
print("replace: privateSub")
print("privateSub:", uid + "@debian.org")
print("-")
print()

print()
print("Maybe paste (or pipe) this into")
print("ldapmodify -ZZ -x -D uid=$USER,ou=users,dc=debian,dc=org -W -h db.debian.org")

# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4:
